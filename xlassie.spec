Summary:	Mail notification tool for X11, POP3 support, counts messages
Name:		xlassie
Version:	1.8
Release:	1
Copyright:	GPL
Group:		X11/Utilities
Packager:	Trent Piepho <xyzzy@speakeasy.org>
Source:		http://www.speakeasy.org/~xyzzy/download/%{name}-%{version}.tar.gz
URL:		http://www.speakeasy.org/~xyzzy/xlassie/
BuildRoot:	/tmp/xlassie

%changelog
* Fri Jan 1 1999 Trent Piepho <xyzzy@u.washington.edu>
- Made spec file

%description
XLassie is an enhanced version of XBiff.  Support for POP3 mailservers,
ability to run a command when clicked on, written in straight xlib so memory
usage is less, and it not only tells you _if_ you have new mail, but how
many new messages you have.  Extra support to operate as a KDE or WindowMaker
dock applet, in addition to plain old X11 mode.

%prep
%setup

%build
make CFLAGS="$RPM_OPT_FLAGS" xlassie xlassie.1x

%install
rm -rf $RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT/usr/X11R6/bin
install -s -m 755 xlassie $RPM_BUILD_ROOT/usr/X11R6/bin
install -d -m 755 $RPM_BUILD_ROOT/usr/X11R6/man/man1
install -s -m 755 xlassie.1x $RPM_BUILD_ROOT/usr/X11R6/man/man1

%files
%attr(-, root, root) %doc README xlassie.lsm
%attr(-, root, root) /usr/X11R6/bin/xlassie
%attr(-, root, root) /usr/X11R6/man/man1/xlassie.1x

%clean
rm -rf $RPM_BUILD_ROOT

/* Default settings */

/* You can change the default colors by editing the source code, they are
 * near the beginning and should be pretty obvious */


/****************************************************************************/

/* Support for getting mail from a POP3 server.  Use the -pop3 option to give
 * the server name, the -password option for the password, and the -usename
 * option if the usename is different than the local one.  The password
 * argument will be overwritten so it doesn't show up in a "ps".  Even if you
 * compile in POP3 support, it is still possible to use a local mail spool. */

#define HAVE_POP3

/****************************************************************************/

/* Make offline mode the default.  Offline mode is usefull if you have a
 * dialup connection to the internet.  XLassie will display an "X" instead of
 * a number if it is unable to contact the POP3 server.  */

/* #define OFFLINE_DEFAULT */

/****************************************************************************/
/* Interval between checks for new mail, the default is 5 seconds for a local
 * file and 60 seconds for a POP3 server */

#define INTERVAL_SPOOL	"5"	/* Spool file default */
#define INTERVAL_REMOTE	"60"	/* POP3 default */

/****************************************************************************/

/* Default command to run when the window is clicked on.  The command will get
 * executed via system() which means you can use shell contructs.  The command
 * should be placed in the background or xlassie will be frozen until it
 * completes. */

/* run pine in an xterm */
/* #define COMMAND	"rxvt +sb -T pine -e pine &" */

/* start kmail */
/* #define COMMAND "kmail &" */

/* fetch mail from pop server */
/* #define COMMAND "fetchpop -r &" */

/* fetch pop mail, then run pine */
/* #define COMMAND "(fetchpop -r ; rxvt -e pine) &" */

/* Don't actually fetch mail, just check mailbox count */
#define COMMAND "CHECK_MAILBOX"

/****************************************************************************/

/* Default font for the display.  You can use xfontsel or gfontview to try and
 * find some nice font on your system.  I would suggest a scalable font. */

/* Some people don't have the utopia font, but they should, it's one of the
 * standard fonts that come with ghostscript.   If desperate just use a wildcard.  */
#define FONTNAME "-*-utopia-medium-r-normal--40-*-*-*-*-*-iso8859-1"
#define KDEFONT	 "-*-utopia-medium-r-normal--25-*-*-*-*-*-iso8859-1"
#define WMFONT   "-*-utopia-medium-r-normal--56-*-*-*-*-*-iso8859-1"

/* These fonts will get used as back ups, in case the first font doesn't work.
 * They will just be tried in order, and if none of them are found, then
 * xlassie will fail to run.  */

#define FONTNAME0 "-*-helvetica-medium-r-normal--40-*-*-*-*-*-iso8859-1"
#define KDEFONT0  "-*-helvetica-medium-r-normal--25-*-*-*-*-*-iso8859-1"
#define WMFONT0   "-*-helvetica-medium-r-normal--56-*-*-*-*-*-iso8859-1"

/* The URW helvetica scalable font, common with Linux distros as the part of
 * the urw-fonts or ghostscript-fonts packages */
#define FONTNAME1 "-urw-helvetica-medium-r-normal--40-*-*-*-*-*-iso8859-1"
#define KDEFONT1 "-urw-helvetica-medium-r-normal--25-*-*-*-*-*-iso8859-1"
#define WMFONT1 "-urw-helvetica-medium-r-normal--56-*-*-*-*-*-iso8859-1"

/* The Helvetica font might also be called nimbus sans l.... */
#define FONTNAME2 "-urw-nimbus sans l-medium-r-normal--40-*-*-*-*-*-iso8859-1"
#define KDEFONT2  "-urw-nimbus sans l-medium-r-normal--25-*-*-*-*-*-iso8859-1"
#define WMFONT2   "-urw-nimbus sans l-medium-r-normal--56-*-*-*-*-*-iso8859-1"

/* Don't have any scalable fonts?  Maybe you have these bitmap fonts? */
#define FONTNAME3 "-adobe-times-medium-r-normal--34-*-*-*-*-*-iso8859-1"
#define KDEFONT3  "-adobe-times-medium-r-normal--25-*-*-*-*-*-iso8859-1"
#define WMFONT3   "-adobe-times-medium-r-normal--34-*-*-*-*-*-iso8859-1"

/* This should work! */
#define FONTNAME4	"-*-*-medium-r-normal--40-*-*-*-*-*-iso8859-1"
#define KDEFONT4	"-*-*-medium-r-normal--25-*-*-*-*-*-iso8859-1"
#define WMFONT4		"-*-*-medium-r-normal--56-*-*-*-*-*-iso8859-1"
